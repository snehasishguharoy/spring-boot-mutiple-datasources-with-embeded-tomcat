package com.demo.example.cru.cnfg;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.MappedTypes;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jndi.JndiObjectFactoryBean;

import com.demo.example.cru.vo.EmpVo;

@Configuration
@MappedTypes(EmpVo.class)
// @MapperScan(basePackageClasses=EmpCssMapperIfc.class,sqlSessionFactoryRef="cssSqlSessionFactory")
public class EmpCssCnfg {

	// @Bean(name = "cssDataSource")
	// @ConfigurationProperties(prefix = "datasource.db-css")
	// @Profile({ "dvl","si","prd" })
	// public DataSource cssDataSource() {
	// return DataSourceBuilder.create().build();
	// }

	@Bean(name = "cssDataSource", destroyMethod = "")
	@Profile({ "dvl", "si", "prd" })
	public DataSource cssDataSource() throws IllegalArgumentException, NamingException {
		JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
		bean.setJndiName("java:comp/env/jdbc/CssDataSource");
		bean.setProxyInterface(DataSource.class);
		bean.setLookupOnStartup(false);
		bean.afterPropertiesSet();
		System.out.println(bean.getObject());
		return (DataSource) bean.getObject();
	}

	@Bean(name = "cssSqlSessionFactory")
	@Profile({ "dvl", "si", "prd" })
	public SqlSessionFactory cssSqlSessionFactoryBean() throws Exception {
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(cssDataSource());
		sqlSessionFactoryBean.setTypeAliasesPackage("com.demo.example.cru.vo");
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath:/EmpCssMapper.xml"));
		return sqlSessionFactoryBean.getObject();
	}

	@Bean
	@Profile({ "dvl", "si", "prd" })
	public TomcatEmbeddedServletContainerFactory tomcatFactory() {
		return new TomcatEmbeddedServletContainerFactory() {

			@Override
			protected TomcatEmbeddedServletContainer getTomcatEmbeddedServletContainer(Tomcat tomcat) {
				tomcat.enableNaming();
				return super.getTomcatEmbeddedServletContainer(tomcat);
			}

			@Override
			protected void postProcessContext(Context context) {
				ContextResource resource = new ContextResource();
				resource.setName("jdbc/CssDataSource");
				resource.setType(DataSource.class.getName());
				resource.setProperty("driverClassName", "org.postgresql.Driver");
				resource.setProperty("url", "jdbc:postgresql://127.0.0.1:5432/employee");
				resource.setProperty("username", "postgres");
				resource.setProperty("password", "password");

				context.getNamingResources().addResource(resource);

				ContextResource resource1 = new ContextResource();
				resource1.setName("jdbc/DvlDataSource");
				resource1.setType(DataSource.class.getName());
				resource1.setProperty("driverClassName", "org.postgresql.Driver");
				resource1.setProperty("url", "jdbc:postgresql://127.0.0.1:5432/postgres");
				resource1.setProperty("username", "postgres");
				resource1.setProperty("password", "password");

				context.getNamingResources().addResource(resource1);

			}
		};
	}

}
