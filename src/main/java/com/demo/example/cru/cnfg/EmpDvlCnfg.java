package com.demo.example.cru.cnfg;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.MappedTypes;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jndi.JndiObjectFactoryBean;

import com.demo.example.cru.vo.EmpVo;

@Configuration
@MappedTypes(EmpVo.class)
// @MapperScan(basePackageClasses=EmpDvlMapperIfc.class,sqlSessionFactoryRef="dvlSqlSessionFactory")
public class EmpDvlCnfg {
//
//	 @Bean(name = "dvlDataSource")
//	 @ConfigurationProperties(prefix = "datasource.db-dvl")
//	 @Profile({ "dvl","si","prd" })
//	 public DataSource dvlDataSource() {
//	 return DataSourceBuilder.create().build();
//	 }

	@Bean(name = "dvlDataSource", destroyMethod = "")
	@Profile({ "dvl", "si", "prd" })
	public DataSource dvlDataSource() throws IllegalArgumentException, NamingException {
		JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
		bean.setJndiName("java:comp/env/jdbc/DvlDataSource");
		bean.setProxyInterface(DataSource.class);
		bean.setLookupOnStartup(false);
		bean.afterPropertiesSet();
		System.out.println(bean.getObject());
		return (DataSource) bean.getObject();
	}

	@Bean(name = "dvlSqlSessionFactory")
	@Profile({ "dvl", "si", "prd" })
	public SqlSessionFactory dvlSqlSessionFactoryBean() throws Exception {
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(dvlDataSource());
		sqlSessionFactoryBean.setTypeAliasesPackage("com.demo.example.cru.vo");
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath:/EmpDvlMapper.xml"));
		return sqlSessionFactoryBean.getObject();
	}

}
