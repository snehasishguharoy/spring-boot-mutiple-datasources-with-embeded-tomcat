package com.demo.example.cru.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.demo.example.cru.excp.DataAcesExcp;
import com.demo.example.cru.mapper.EmpCssMapperIfc;
import com.demo.example.cru.vo.EmpVo;
@Repository
public  class CssEmpDaoImpl implements CssempDaoIfc {
	
	@Autowired
	@Qualifier("cssSqlSessionFactory")
	private SqlSessionFactory cssSqlSessionFactory;

	@Override
	public List<EmpVo> cssGetEmpVos() throws DataAcesExcp {
		try {
			SqlSession sqlSession=cssSqlSessionFactory.openSession();
			EmpCssMapperIfc empMapperIfc=sqlSession.getMapper(EmpCssMapperIfc.class);
			
			return empMapperIfc.cssGetEmpVos();
		} catch (DataAccessException dataAccessException) {
			throw new DataAcesExcp(dataAccessException);
		}
	}

}
