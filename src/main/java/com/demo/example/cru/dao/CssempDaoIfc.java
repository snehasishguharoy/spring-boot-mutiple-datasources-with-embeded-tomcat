package com.demo.example.cru.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.demo.example.cru.excp.DataAcesExcp;
import com.demo.example.cru.vo.EmpVo;

public interface CssempDaoIfc {
	//int empInsrt(EmpVo empVo) throws DataAcesExcp;

	List<EmpVo> cssGetEmpVos() throws DataAcesExcp;
	
	

	//int empUpdt(EmpVo empVo) throws DataAcesExcp;

	//EmpVo fndEmpById(Integer empId) throws DataAcesExcp;

	//Integer deleteEmp(Integer empId) throws DataAcesExcp;

}
