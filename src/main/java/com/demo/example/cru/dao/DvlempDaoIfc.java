package com.demo.example.cru.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.demo.example.cru.excp.DataAcesExcp;
import com.demo.example.cru.vo.EmpVo;

public interface DvlempDaoIfc {
	List<EmpVo> dvlGetEmpVos() throws DataAcesExcp;
}
