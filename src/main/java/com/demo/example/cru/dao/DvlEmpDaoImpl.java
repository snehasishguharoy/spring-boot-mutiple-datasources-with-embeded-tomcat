package com.demo.example.cru.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.demo.example.cru.excp.DataAcesExcp;
import com.demo.example.cru.mapper.EmpDvlMapperIfc;
import com.demo.example.cru.vo.EmpVo;
@Repository
public class DvlEmpDaoImpl implements DvlempDaoIfc{
	
	@Autowired
	@Qualifier("dvlSqlSessionFactory")
	private SqlSessionFactory dvlSqlSessionFactory;
	@Override
	public List<EmpVo> dvlGetEmpVos() throws DataAcesExcp {
		try {
			SqlSession sqlSession=dvlSqlSessionFactory.openSession();
			EmpDvlMapperIfc empMapperIfc=sqlSession.getMapper(EmpDvlMapperIfc.class);
			
			return empMapperIfc.dvlGetEmpVos();
		} catch (DataAccessException dataAccessException) {
			throw new DataAcesExcp(dataAccessException);
		}
	}



}
