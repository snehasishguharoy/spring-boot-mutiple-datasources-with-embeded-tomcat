package com.demo.example.cru.mapper;

import java.util.List;

import org.springframework.stereotype.Component;

import com.demo.example.cru.vo.EmpVo;
@Component
public interface EmpDvlMapperIfc {
	
	List<EmpVo> dvlGetEmpVos();

}
