package com.demo.example.cru.cntlr;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.example.cru.bo.EmpBoImpl;
import com.demo.example.cru.excp.BsnObjException;
import com.demo.example.cru.excp.DataAcesExcp;
import com.demo.example.cru.vo.EmpVo;

@RestController
public class EmpCtlr {
	@Autowired
	private EmpBoImpl boImpl;
	private static final Logger LOGGER = Logger.getLogger(EmpCtlr.class.getName());
//
//	@RequestMapping(value = "/insert", method = RequestMethod.POST, consumes = { "application/json" })
//	public ResponseEntity<Integer> empInsrt(@RequestBody EmpVo empVo) {
//		LOGGER.info("Inside the Controller Method");
//		Integer message=0;
//		try {
//			message = boImpl.empInsrt(empVo);
//		} catch (BsnObjException e) {
//			e.getMessage();
//		}
//		HttpStatus httpStatus = message != null ? HttpStatus.OK : HttpStatus.SERVICE_UNAVAILABLE;
//
//		return new ResponseEntity<Integer>(message, httpStatus);
//
//	}

	@RequestMapping(value = "/cssallemps", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<List<EmpVo>> cssGetEmpVos() throws DataAcesExcp {
		LOGGER.info("Inside the Controller Method");
		List<EmpVo> empVos=null;
		empVos = boImpl.cssGetEmpVos();
		HttpStatus httpStatus = empVos != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
		return new ResponseEntity<List<EmpVo>>(empVos, httpStatus);
	}
	
	@RequestMapping(value = "/dvlallemps", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<List<EmpVo>> dvlGetEmpVos() throws DataAcesExcp {
		LOGGER.info("Inside the Controller Method");
		List<EmpVo> empVos=null;
		empVos = boImpl.dvlGetEmpVos();
		HttpStatus httpStatus = empVos != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
		return new ResponseEntity<List<EmpVo>>(empVos, httpStatus);
	}
//
//	@RequestMapping(value = "/insert", method = RequestMethod.PUT, consumes = { "application/json" })
//	public ResponseEntity<Integer> empUpdt(@RequestBody EmpVo empVo) {
//		LOGGER.info("Inside the Controller Method");
//		Integer message=null;
//		try {
//			message = boImpl.empUpdt(empVo);
//		} catch (BsnObjException e) {
//			e.getMessage();
//		}
//		HttpStatus httpStatus = message != null ? HttpStatus.OK : HttpStatus.SERVICE_UNAVAILABLE;
//		return new ResponseEntity<Integer>(message, httpStatus);
	
	
	
	
//	}
//
//	@RequestMapping(value = "/emp/{empId}", method = RequestMethod.GET, consumes = { "application/json" })
//	public ResponseEntity<EmpVo> fndEmpById(@PathVariable("empId") Integer empId) {
//
//		LOGGER.info("Inside the Controller Method");
//		EmpVo empVo=null;
//		try {
//			empVo = boImpl.fndEmpById(empId);
//		} catch (BsnObjException e) {
//			e.getMessage();
//		}
//		HttpStatus httpStatus = empVo != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
//		return new ResponseEntity<EmpVo>(empVo, httpStatus);
//
//	}
//
//	@RequestMapping(value = "/del/{empId}", method = RequestMethod.DELETE, consumes = { "application/json" })
//	public ResponseEntity<Integer> deleteEmp(@PathVariable("empId") Integer empId) {
//
//		LOGGER.info("Inside the Controller Method");
//		Integer message=null;
//		try {
//			message = boImpl.deleteEmp(empId);
//		} catch (BsnObjException e) {
//			e.getMessage();
//		}
//		HttpStatus httpStatus = message != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
//		return new ResponseEntity<Integer>(message, httpStatus);
//
//	}


}
