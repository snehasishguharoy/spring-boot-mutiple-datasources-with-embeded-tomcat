package com.demo.example.cru.bo;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.example.cru.dao.CssEmpDaoImpl;
import com.demo.example.cru.dao.DvlEmpDaoImpl;
import com.demo.example.cru.excp.DataAcesExcp;
import com.demo.example.cru.vo.EmpVo;

@Service
public class EmpBoImpl implements EmpBoIfc {
	private static final Logger LOGGER = Logger.getLogger(EmpBoImpl.class.getName());
	@Autowired
	private CssEmpDaoImpl empDaoIfc;
	@Autowired
	private DvlEmpDaoImpl dvlEmpDaoImpl;
	
	@Override
	public List<EmpVo> cssGetEmpVos() throws DataAcesExcp {
		return empDaoIfc.cssGetEmpVos();
	}
	@Override
	public List<EmpVo> dvlGetEmpVos() throws DataAcesExcp {
		return dvlEmpDaoImpl.dvlGetEmpVos();
	}

	

}
