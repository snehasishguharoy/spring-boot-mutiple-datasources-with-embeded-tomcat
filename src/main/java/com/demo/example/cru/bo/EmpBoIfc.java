package com.demo.example.cru.bo;

import java.util.List;

import com.demo.example.cru.excp.BsnObjException;
import com.demo.example.cru.excp.DataAcesExcp;
import com.demo.example.cru.vo.EmpVo;

public interface EmpBoIfc {

	List<EmpVo> cssGetEmpVos() throws DataAcesExcp;
	
	List<EmpVo> dvlGetEmpVos() throws DataAcesExcp;


}
