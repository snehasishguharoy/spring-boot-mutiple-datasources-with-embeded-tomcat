package com.demo.example.cru.unttest.excp;

import com.demo.example.cru.excp.BsnObjException;

public class Thrower {
	
	  public void throwsBsnObjException(Throwable t) throws BsnObjException {
	        throw new BsnObjException(t);
	    }

}
