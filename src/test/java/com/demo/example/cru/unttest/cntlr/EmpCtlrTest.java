package com.demo.example.cru.unttest.cntlr;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.demo.example.cru.bo.EmpBoImpl;
import com.demo.example.cru.cntlr.EmpCtlr;
import com.demo.example.cru.excp.BsnObjException;
import com.demo.example.cru.vo.EmpVo;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class EmpCtlrTest {

	@InjectMocks
	private EmpCtlr ctlr;
	@Mock
	private EmpBoImpl boImpl;

	@Test
	public void empInsrtTest() throws BsnObjException {

		EmpVo empVo = new EmpVo();

		Mockito.when(ctlr.empInsrt(Mockito.isA(EmpVo.class))).thenThrow(new BsnObjException(new Exception()));
		ctlr.empInsrt(empVo);

	}

	@Test
	public void getEmpVosTest() throws BsnObjException {

		EmpVo empVo = new EmpVo();

		Mockito.when(ctlr.getEmpVos()).thenThrow(new BsnObjException(new Exception()));
		ctlr.getEmpVos();

	}

	@Test
	public void empUpdtTest() throws BsnObjException {

		EmpVo empVo = new EmpVo();

		Mockito.when(ctlr.empUpdt(Mockito.isA(EmpVo.class))).thenThrow(new BsnObjException(new Exception()));
		ctlr.empUpdt(empVo);

	}

	@Test
	public void fndEmpByIdTest() throws BsnObjException {

		Integer integer = new Integer(10);

		Mockito.when(ctlr.fndEmpById(Mockito.isA(Integer.class))).thenThrow(new BsnObjException(new Exception()));
		ctlr.fndEmpById(integer);

	}
	
	@Test
	public void deleteEmpTest() throws BsnObjException {

		Integer integer = new Integer(10);

		Mockito.when(ctlr.deleteEmp(Mockito.isA(Integer.class))).thenThrow(new BsnObjException(new Exception()));
		ctlr.deleteEmp(integer);

	}

}
