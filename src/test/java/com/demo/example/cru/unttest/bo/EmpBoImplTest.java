package com.demo.example.cru.unttest.bo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.demo.example.cru.bo.EmpBoImpl;
import com.demo.example.cru.dao.EmpDaoImpl;
import com.demo.example.cru.excp.BsnObjException;
import com.demo.example.cru.excp.DataAcesExcp;
import com.demo.example.cru.vo.EmpVo;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class EmpBoImplTest {
	
	@InjectMocks
	private EmpBoImpl boImpl;

	@Mock
	private EmpDaoImpl daoImpl;
	
	@Test(expected=BsnObjException.class)
	public void empInsrtTest() throws BsnObjException{
		
		EmpVo empVo = new EmpVo();
		Mockito.when(boImpl.empInsrt(Mockito.isA(EmpVo.class))).thenThrow(new DataAcesExcp(new Exception()));
		boImpl.empInsrt(empVo);
		
	}
	
	@Test(expected=BsnObjException.class)
	public void empInsrtTest_1() throws BsnObjException{
		
		EmpVo empVo = new EmpVo();
		Mockito.when(boImpl.empInsrt(Mockito.isA(EmpVo.class))).thenThrow(new ArithmeticException());
		boImpl.empInsrt(empVo);
		
	}
	
	
	

}
